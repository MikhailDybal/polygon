"use strict";

var isLogged = false;

function logOut(){
    console.log('log out');
    isLogged = false;
    $('.btn-login').show();
    $('.btn-logout').hide();
    $('.btn-personal').hide();
}

function logIn(){
    console.log('log in');
    isLogged = true;
    $('.btn-login').hide();
    $('.btn-logout').show();
    $('.btn-personal').show();
    $('#message_center>.alert-dismissable').show();
    $('#loginModal').modal('hide');//temp TODO
    setTimeout(function(){$('#message_center>.alert-dismissable').hide();},3000);
}

function showSendMessageModal(){
    if(isLogged){
        $('#sendMessageModal').modal('show')
    } else {
        $('#loginModal').modal('show')
    }
}

function checkLoginOnDocumentReady() {
    console.log('log in');
    // тут типа проверка сисурити по нужным технологиям
    if(false){
        logIn();
    } else {
        logOut();
    }
}

function sendMessage() {
    $('.messagebar .link').on('click', function () {
        // Message text
        var messageText = $('.toolbar-inner textarea').val().trim();
        var myMessagebar = $('.toolbar-inner textarea');
        // Exit if empy message
        if (messageText.length === 0) return;
        // Empty messagebar
        myMessagebar.val("");
        $('.messages').append('<div class="message message-sent"><div class="message-text">' + messageText + '</div> </div>');
        var heightMessageContent = $(".messages").height();
        $('.messages-content').scrollTop(+heightMessageContent);
        //$('.messages-content').scroll
    });
}

function initMap() {
    var map;
    var adCords = {lat: 59.916595, lng: 30.290922};
    map = new google.maps.Map(document.getElementById('map'), {
        center: adCords,
        zoom: 10
    });

    var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h4 id="firstHeading" class="firstHeading">Заголовок</h4>'+
        '<div id="bodyContent">'+
        '<p><b>Абзац</b><br>Подробности'+
        '</div>'+
        '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    var marker = new google.maps.Marker({
        position: adCords,
        map: map,
        title: 'Некоторый адрес'
    });
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
}

$(document).ready(function(e) {
    $('.messages-content').addClass('active');
    $('.messagebar').addClass('active');

    checkLoginOnDocumentReady();
});