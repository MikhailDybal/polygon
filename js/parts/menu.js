"use strict";

var toogleSidebarUl = function (e, context) {
    e.stopPropagation();
    if($(context).parent().has("ul")) {
        if(!$(context).hasClass("open")) {
            // hide any open menus and remove all other classes
            $(".ul-wrap li ul").slideUp(350);
            $(".ul-wrap li a").removeClass("open");

            // open our new menu and add the open class
            $(context).next("ul").slideDown(350);
            $(context).addClass("open");
        }
        else {
            $(context).removeClass("open");
            $(context).next("ul").slideUp(350);
        }
    }
};

$(document).ready(function(e){

    /*sidebar->items-> toogle sub-items*/
    $(".sidebar .menu-wrap .ul-wrap > li > a").on("click", function(e){
        toogleSidebarUl(e, this);
    });

    /*sidebar->items-> toogle sub-items*/
    // $(".sidebar .menu-wrap .ul-wrap > li > a").on("mouseenter", function(e){
    //     if($( window ).width()>992){
    //         toogleSidebarUl(e, this);
    //     }
    // });

    /* toogle sidebar when resize window*/
    $( window ).resize(function() {
        if($( window ).width()>992){
            $("main .sidebar").css('display','inline-block');
        }else {
            $("main .sidebar").css('display','none');
        }
    });
    $(window).on("click", function(e) {
        if($("header .navbar-header>.navbar-toggle")[0].nodeName != e.target.nodeName){
            if($( window ).width()<992) {
                $("main .sidebar").css('display', 'none');
                e.stopPropagation();
            }
        }
    });

    $("header .navbar-header>.navbar-toggle").on("click", function(e){
        $(".sidebar").slideToggle('fast', function() {
            if ($(this).is(':visible')){
                $(this).css('display','inline-block');
            }
        });
        e.stopPropagation();
    });

});


