"use strict";
sendMessage();

$('.contacts-content .contact').on("click", function(e) {
    $('.contacts-content .contact').removeClass("active");
    $(this).removeClass("new-message");
    $(this).find(".new-message-quantity").hide();
    $(this).addClass("active");
});

$('.posters .poster').on("click", function() {
    $('.posters .poster').removeClass("active");
    $(this).addClass("active");
});

$('.posters-header').on("click", function() {
    $('.posters').toggleClass('active');
});

$('.contacts .navbar').on("click", function() {
   // $('.contacts-content').toggleClass('active');
});

$('.dialogs .navbar').on("click", function() {
  //  $('.messages-content').toggleClass('active');
  //  $('.messagebar').toggleClass('active');
});

$('.contact-delete').on("click", function(e){
    e.preventDefault();
    e.stopPropagation();
});

$('.contacts .contact-goto').on("click", function(e){
    window.location.href = '/personal-page-with-dialog.html';
});

$('.dialogs .navbar-back').on("click", function(e){
    window.location.href = '/personal-page.html';
});

function sendMessage() {
    $('.messagebar .link').on('click', function () {
        // Message text
        var messageText = $('.toolbar-inner textarea').val().trim();
        var myMessagebar = $('.toolbar-inner textarea');
        // Exit if empy message
        if (messageText.length === 0) return;

        // Empty messagebar
        myMessagebar.val("");

        $('.messages').append('<div class="message message-sent"><div class="message-text">' + messageText + '</div> </div>');
        var heightMessageContent = $(".messages").height();
        $('.messages-content').scrollTop(+heightMessageContent);

        //$('.messages-content').scroll
    });
}

function add_phone_mask() {
    var phone = $('input[name=phone]');
    // if (phone.length) {
    phone.each(function() {
        $(this).inputmask({
            'alias' : 'phone'
        });
//             var phn = $(this);
//             phn.mask("+7 (999) 999 9999");
//             //phn.mask("99/99/9999",{placeholder:" "});
    });
    // };
};

$(function() {
    $('.toggle-enabled-ad').bootstrapToggle({
        on: 'Активно',
        off: 'Неактивно',
        onstyle: 'success',
        width: 120
    });
})

$(document).ready(function(e) {
    add_phone_mask();
});