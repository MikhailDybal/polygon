"use strict";

function prepareSlider(){
    $('.pgwSlider').pgwSlider({
        autoSlide : false,
        adaptiveHeight: true,
        maxHeight : 600
    });

    if($('.pgwSlider li').length <= 2 ) {
        document.getElementsByClassName('ps-current')[0].style.width = '100%';
        document.getElementsByClassName('ps-list')[0].style.display = 'none';
    }
}

$(document).ready(function() {
    prepareSlider();
});
