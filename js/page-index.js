"use strict";

var closeFiltersPanel = function (e) {
    $(".filter-wrap").css('display','none');
    $('.overlay').slideToggle('fast', function() {
        $('.overlay').css('display','none');
    });
    e.stopPropagation();
    $("html,body").css("overflow","auto");
};

var toogleSidebarUl = function (e, context) {
    e.stopPropagation();
    if($(context).parent().has("ul")) {
        if(!$(context).hasClass("open")) {
            // hide any open menus and remove all other classes
            $(".ul-wrap li ul").slideUp(350);
            $(".ul-wrap li a").removeClass("open");

            // open our new menu and add the open class
            $(context).next("ul").slideDown(350);
            $(context).addClass("open");
        }
        else {
            $(context).removeClass("open");
            $(context).next("ul").slideUp(350);
        }
    }
};

function fixSearchWrap(marginRight){
    var winWidth = $( window ).width();
    var mr = $('.input-group.search-wrap').css("margin-right")
    if(winWidth>750 && winWidth<992) {
        $('.input-group.search-wrap').css("margin-right",marginRight+"px")
    } else {
        $('.input-group.search-wrap').css("margin-right", mr )
    }
}


$(document).ready(function(e){
    $(".worker-wrap").mouseenter(function(){
        $(".worker-wrap").animate({"left": "+=100px"}, "slow");
    });
    $(".worker-wrap").mouseleave(function(){
        $(".worker-wrap").animate({"left": "-=100px"}, "slow");
    });

    /*sidebar->items-> toogle sub-items*/
    $(".sidebar .menu-wrap .ul-wrap > li > a").on("click", function(e){
        toogleSidebarUl(e, this);
    });

    /*sidebar->items-> toogle sub-items*/
    // $(".sidebar .menu-wrap .ul-wrap > li > a").on("mouseenter", function(e){
    //     if($( window ).width()>992){
    //         toogleSidebarUl(e, this);
    //     }
    // });

    /* toogle sidebar when resize window*/
    $( window ).resize(function() {
        if($( window ).width()>992){
            $("main .sidebar").css('display','inline-block');
        }else {
            $("main .sidebar").css('display','none');
        }
    });
    $(window).on("click", function(e) {
        if($("header .navbar-header>.navbar-toggle")[0].nodeName != e.target.nodeName){
            if($( window ).width()<992) {
                $("main .sidebar").css('display', 'none');
                e.stopPropagation();
            }
        }
    });

    $("header .navbar-header>.navbar-toggle").on("click", function(e){
        $(".sidebar").slideToggle('fast', function() {
            if ($(this).is(':visible')){
                $(this).css('display','inline-block');
            }
        });
        e.stopPropagation();
    });

    /* click filters toogle*/
    $("main .main-content .section>.section-header>.filters-wrap>.filter-button-wrap>a").on("click ", function(e){
        $(".filter-wrap").slideToggle('fast', function() {
            if ($(this).is(':visible')){
                $(this).css('display','block');
            }
        });
        $(".overlay").slideToggle('fast', function() {
            $('.overlay').css('display','block');
        });
        e.stopPropagation();
        $("html,body").css("overflow","hidden");
        //   $("main .filter-wrap").css("overflow","auto");

    });
    $(".filter-wrap>.filter-header .button-close").on("click", function(e){
        closeFiltersPanel(e);
    });
    $("main  .overlay").on("click touchstart", function(e){
        closeFiltersPanel(e);
    });
    $("main .filter-wrap .filter-controls .reset-filters").on("click", function(e){
        closeFiltersPanel(e);
    });
    $("main .filter-wrap .filter-controls .set-filters").on("click", function(e){
        closeFiltersPanel(e);
    });

    //checkLoginOnDocumentReady();
});

function init () {
    /*
    var myMap = new ymaps.Map("map", {
        center: [55.76, 37.64],
        zoom: 10
    });
    myMap.controls
        .add('zoomControl', { left: 5, top: 5 })
        .add('typeSelector');
    myMap.controls
    */
}


//ymaps.ready(init);

function clickTabmap(){

    setTimeout(function(){
        $('#map').css('height', $('#map').css('height'));
        google.maps.event.trigger(map, 'resize');
    }, 1000);
}



