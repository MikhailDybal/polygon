"use strict";

var toogleSidebarUl = function (e, context) {
    e.stopPropagation();
    if($(context).parent().has("ul")) {
        if(!$(context).hasClass("open")) {
            // hide any open menus and remove all other classes
            $(".ul-wrap li ul").slideUp(350);
            $(".ul-wrap li a").removeClass("open");

            // open our new menu and add the open class
            $(context).next("ul").slideDown(350);
            $(context).addClass("open");
        }
        else {
            $(context).removeClass("open");
            $(context).next("ul").slideUp(350);
        }
    }
};

$(document).ready(function() {
    $('.pgwSlider').pgwSlider({autoSlide : false});
});

ymaps.ready(init);
function init () {
    var myMap = new ymaps.Map("map", {
        center: [55.76, 37.64],
        zoom: 10
    });
    var myPlacemark1 = new ymaps.Placemark([55.8, 37.6], {
        iconContent: '',
        balloonContent: 'Адрес'
    }, {
        preset: 'twirl#violetIcon'
    });
    myMap.geoObjects
        .add(myPlacemark1);
}

$(document).ready(function(e) {
    /*sidebar->items-> toogle sub-items*/
    $(".sidebar .menu-wrap .ul-wrap > li > a").on("click", function (e) {
        toogleSidebarUl(e, this);
    });
    /*sidebar->items-> toogle sub-items*/
    $(".sidebar .menu-wrap .ul-wrap > li > a").on("mouseenter", function (e) {
        if ($(window).width() > 992) {
            toogleSidebarUl(e, this);
        }
    });

    /* toogle sidebar when resize window*/
    $( window ).resize(function() {
        $("main .sidebar").css('display','none');
    });

    /* toogle sidebar when resize window*/
    $(window).on("click", function (e) {
        if ($("header .navbar-header>.navbar-toggle")[0].nodeName != e.target.nodeName) {
            if ($(window).width() < 992) {
                $("main .sidebar").css('display', 'none');
                e.stopPropagation();
            }
        }
    });

    $("header .navbar-header>.navbar-toggle").on("click", function (e) {
        $(".sidebar").slideToggle('fast', function () {
            if ($(this).is(':visible')) {
                $(this).css('display', 'inline-block');
            }
        });
        e.stopPropagation();
    });

});