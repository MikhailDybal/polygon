"use strict";

$(document).ready(function() {
    $('.pgwSlider').pgwSlider({autoSlide : false});
});

ymaps.ready(init);
function init () {
    var myMap = new ymaps.Map("map", {
        center: [55.76, 37.64],
        zoom: 10
    });
    var myPlacemark1 = new ymaps.Placemark([55.8, 37.6], {
        iconContent: '',
        balloonContent: 'Адрес'
    }, {
        preset: 'twirl#violetIcon'
    });
    myMap.geoObjects
        .add(myPlacemark1);
}